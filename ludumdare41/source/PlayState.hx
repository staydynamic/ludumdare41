package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.util.FlxCollision;
import flixel.system.FlxSound;
import flixel.system.scaleModes.RelativeScaleMode;


class PlayState extends FlxState
{
	public static var statusMessage:String = "ld41";
	public static var gameMessage:FlxText;
	
	private static var _player:Player;

  /**
   * The various food stations in the diner
   */
  private var _stations:FlxTypedGroup<Station>;
  private var _tables:FlxTypedGroup<Table>;
  private var _foodHUD:Array<FlxSprite>;
	private var _enemyManager:EnemyManager;
	private var _enemies:FlxGroup;
	private var _entrance:FlxSprite;

	private var _pizzaStand_x:Int = 600;
	private var _pizzaStand_y:Int = 100;
	private var _sandStand_x:Int = 800;
	private var _sandStand_y:Int = 100;
	private var _sodaStand_x:Int = 1000;
	private var _sodaStand_y:Int = 100;

	private var _pizzaHUD_x:Int = 10;
	private var _pizzaHUD_y:Int = 10;
	private var _sandHUD_x:Int = 100;
	private var _sandHUD_y:Int = 10;
	private var _sodaHUD_x:Int = 180;
	private var _sodaHUD_y:Int = 10;

	private var _baseTableX:Int = 300;
	private var _baseTableY:Int = 480;
	private var _baseTableY2:Int = 280;

  private var _gameOver = false;
  private var _inCombat = false;

  private var _customersServed:Int = 0;
  private var _timeSurvived:Float = 0;

	public static var _entranceX:Int = 50;
	public static var _entranceY:Int = 75;
	
  private static var sfxLoaded:Bool = false;
  private static var _pickupSfx:FlxSound;
  private static var _serveSfx:FlxSound;

  public static function loadSfx():Void
  {
    _pickupSfx = FlxG.sound.load("assets/pickup-item.ogg");
    _serveSfx = FlxG.sound.load("assets/putdown-item.ogg");

    Player.loadSfx();
    Customer.loadSfx();
    Station.loadSfx();
  }


	public function addToWorld(obj:FlxObject):Void
	{
		_enemies.add(obj);
	}

	public static function getPlayer():Player
	{
		return _player;
	}
	
	
	override public function create():Void
	{
		// Init game settings
		//Background settings
        var bg:FlxSprite = new FlxSprite(0,0);
        bg.loadGraphic("assets/bg.png");

        _entrance = new FlxSprite(_entranceX, _entranceY);
        _entrance.loadGraphic("assets/door.png");

		// Create game objects
		_tables = new FlxTypedGroup<Table>(6);

		var a:Table;
		for (i in 0...6)
		{
			a = new Table(_baseTableX+(_baseTableX*(i%3)), (i > 2)? _baseTableY : _baseTableY2);
			_tables.add(a);
		}

		_player = new Player();
		_enemyManager = new EnemyManager(this, _tables, _player);
		_enemies = new FlxTypedGroup();
    _stations = new FlxTypedGroup<Station>(3);
    _stations.add(new Station(_pizzaStand_x, _pizzaStand_y, 0));
    _stations.add(new Station(_sandStand_x, _sandStand_y, 1));
    _stations.add(new Station(_sodaStand_x, _sodaStand_y, 2));
    _foodHUD = [new FlxSprite(_pizzaHUD_x, _pizzaHUD_y, "assets/pizza.png"), 
                new FlxSprite(_sandHUD_x, _sandHUD_y, "assets/sandwich.png"),
                new FlxSprite(_sodaHUD_x, _sodaHUD_y, "assets/soda.png")];

		// Add objects to game state
		add(bg);
		add(_entrance);
		add(_tables);
    add(_enemyManager);
    add(_enemies);
		add(_stations);
		
		add(_player);
		
		// Then we're going to add a text field to display the label we're storing in the scores array.
		PlayState.gameMessage = new FlxText(16, 16, FlxG.width - 8, statusMessage, 22);
		PlayState.gameMessage.alignment = CENTER;
		//add(PlayState.gameMessage);

    // load sounds too
    PlayState.loadSfx();
    FlxG.sound.playMusic("assets/caf-them.ogg");

    // reset number of hostile enemies
    Customer.numHostile = 0;
    Customer.numDefeated = 0;
	}

	
	/**
	 * This is the main game loop function, where all the logic is done.
	 */
	override public function update(elapsed:Float):Void
	{
		// Check Collisions
		FlxG.overlap(_player, _enemies, playerHitCustomer);
    FlxG.overlap(_player, _stations, playerVisitStation);
    FlxG.overlap(_player, _tables, playerWaitsTable);
    FlxG.overlap(_enemies, _tables, customerOrdersFood);
    FlxG.overlap(_entrance, _enemies, deleteCustomer);
    	
		// Update game state
		super.update(elapsed);
		
    /*
		// Check for reseting the game
		if (FlxG.keys.anyJustPressed([R]))
		{
			statusMessage = "Reset Game";
			FlxG.resetState();
		}
		if(FlxG.keys.pressed.C){
			_enemyManager.createEnemy();
		}
    */

    _timeSurvived += elapsed;

    checkMusic();
    // check for game over
    if(_player.hitPoints <= 0 &&
       !_gameOver)
    {
      persistentUpdate = true;
      var gos = new GameOverState();
      gos.served = _customersServed;
      gos.time = _timeSurvived;
      _gameOver = true;
      openSubState(gos);
    }
	}

  private function checkMusic():Void
  {
    var hasHostiles:Bool = Customer.numHostile > 0;

    if(hasHostiles && !_inCombat)
    {
      FlxG.sound.playMusic("assets/batl-them.ogg");
      _inCombat = true;
    }
    if(!hasHostiles && _inCombat)
    {
      FlxG.sound.playMusic("assets/caf-them.ogg");
      _inCombat = false;
    }
  }

	/**
	 * Check collision on player and customers
	 */
	private function playerHitCustomer(waiter:Player, customer:Customer):Void
	{
		if(!(FlxCollision.pixelPerfectCheck(waiter, customer)))
		{ return; }

		//If we are both punching
		if(customer.animation.name == "punching" &&
			    waiter.animation.name == "swing" && 
			    waiter._invuln <= 0)
		{
			waiter.getHit(waiter.x > customer.x);

			customer.getHit();
		}
		//If the customer is punching us and we are not punching
		else if(customer.animation.name == "punching" &&
				!(waiter.animation.name == "swing") && 
				waiter._invuln <= 0)
		{
			waiter.getHit(waiter.x > customer.x);
		}
		// If we are punching the customer and the customer is angry
		else if( (customer.animation.name == "angry" || 
				  customer.animation.name == "windingUpPunch" ) &&
			    waiter.animation.name == "swing")
		{
			customer.getHit();
		}
		
	}


	private function deleteCustomer(entrance:FlxSprite, customer:Customer):Void
	{
		if(customer.animation.name == "walking" && customer._satiated){
			customer.kill();
      _customersServed += 1;
		}
	}


	

  private function playerVisitStation(player:Player, station:Station):Void
  {
  	if(!(FlxCollision.pixelPerfectCheck(player, station)))
	{ return; }

    var foodType = station.foodType;
    if(player.animation.name == "swing" &&
       station.animation.name == "ready" &&
       player.wantsFood(foodType))
    {
      station.resetCooldown();
      player.hasFood[foodType] = true;
      add(_foodHUD[foodType]);
      PlayState._pickupSfx.play();
    }
  }

  private function playerWaitsTable(player:Player, table:Table): Void
  {
  	if(!(FlxCollision.pixelPerfectCheck(player, table)))
	{ return; }

    var customer = table.getCustomer();
    if(customer != null &&
       customer.animation.name == "seated")
    {
      var needsUpdate = false;
      for(i in 0...3)
      {
        if(customer.desires[i] && player.hasFood[i])
        {
          customer.desires[i] = false;
          player.hasFood[i] = false;
          remove(_foodHUD[i]);
          needsUpdate = true;

          PlayState._serveSfx.play();
        }
      }
      if(needsUpdate && customer.desires.indexOf(true) >= 0)
      {
        customer.clearOrder();
      }
    }
  }

  private function customerOrdersFood(customer:Customer, table:Table): Void
  {

    if(customer.animation.name == "seated" &&
       customer.hasNotOrdered())
    {
      add(customer.placeOrder());
    }
  }

}