package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;

class TitleState extends FlxState
{
	private var _txtTitle:FlxText;
	private var _txtInstructions:FlxText;
	
	override public function create():Void
	{
		if (FlxG.sound.music == null)
		{
      FlxG.sound.playMusic("assets/cafperconly.ogg");
		}
		FlxG.mouse.visible = false;
		
		//_txtTitle = new FlxText(0, 20, 0, "DINER\nBASH", 50);
		//_txtTitle.alignment = CENTER;
		//_txtTitle.screenCenter(FlxAxes.X);
		//add(_txtTitle);

		var logo:FlxSprite = new FlxSprite(0,0);
        logo.loadGraphic("assets/logo.png");
        add(logo);

		_txtInstructions = new FlxText(0, 440, 0, "Press any key to start", 22);
		_txtInstructions.alignment = CENTER;
		_txtInstructions.screenCenter(FlxAxes.X);
		add(_txtInstructions);
		
		FlxG.camera.fade(FlxColor.BLACK, .33, true);
		
		super.create();
	}


	override public function update(elapsed:Float):Void
  {
    super.update(elapsed);

    if(FlxG.keys.firstJustPressed() != -1)
    {
      FlxG.camera.fade(FlxColor.BLACK, .33, false, function()
      {
        FlxG.switchState(new PlayState());
      });
    }
  }
}