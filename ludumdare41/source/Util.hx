package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxSpriteUtil;
import flixel.math.FlxMath;
import flixel.addons.util.FlxFSM;
import flixel.FlxObject;

class Util{

	public static var _PLAYER_RECOVERY_SPEED:Float = 1.5;
	public static var _PLAYER_HITPOINTS:Int = 3;
	public static var _PLAYER_INVULN:Float = 2;
	public static var _PLAYER_KNOCKBACK_DIST:Float = 300;
	public static var _PLAYER_MOVEMENT_SPEED:Float = 100;

	public static var _CUSTOMER_MOVESPEED_X:Int = 40;
	public static var _CUSTOMER_MOVESPEED_Y:Int = 30;
	public static var _PUNCH_DISTANCE:Int = 20;
	
	public static var _CUSTOMER_KNOCKBACK_DIST:Float = 250;
	
	


	public static function findMax(x:Float, y:Float){
		return (x>y) ? x : y;
	}

	public static function findMin(x:Float, y:Float){
		return (x>y) ? y : x;
	}

	public static function findAbsMin(x:Float, y:Float){
		if(x < 0 && y > 0){
			y = -y;
		}
		return (Util.abs(x) > Util.abs(y)) ? y : x;
	}

	public static function abs(x:Float){
		return (x < 0) ? -x : x;
	}

	public static function moveToTarget(owner:Customer){
		owner.velocity.x = 0;
		owner.velocity.y = 0;
		
		// movement using _math_ and _trigonometry_
		owner.velocity.x = Util.findAbsMin((owner.targetX-owner.x)+1,
										owner._MOVESPEED_X);

		owner.velocity.y = Util.findAbsMin((owner.targetY-owner.y)+1,
										owner._MOVESPEED_Y);

		owner.facing = owner.velocity.x <= 0 ? FlxObject.LEFT : FlxObject.RIGHT;
	}

	public static function checkMove(owner:Player){
		owner.velocity.x = 0;
		owner.velocity.y = 0;

		
		
		if (FlxG.keys.anyPressed([LEFT, A]))
		{
			owner.velocity.x -= _PLAYER_MOVEMENT_SPEED;
		}
		if (FlxG.keys.anyPressed([RIGHT, D]))
		{
			owner.velocity.x += _PLAYER_MOVEMENT_SPEED;
		}
		if (FlxG.keys.anyPressed([UP, W]))
		{
			owner.velocity.y -= _PLAYER_MOVEMENT_SPEED;
		}
		if (FlxG.keys.anyPressed([DOWN, S]))
		{
			owner.velocity.y += _PLAYER_MOVEMENT_SPEED;
		}
		if(owner.velocity.x < 0)
			owner.facing = FlxObject.LEFT;
		if(owner.velocity.x > 0){
			owner.facing = FlxObject.RIGHT;
		} 
	}
}