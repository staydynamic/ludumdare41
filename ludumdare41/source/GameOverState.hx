package;

import flixel.FlxG;
import flixel.FlxSubState;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;

class GameOverState extends FlxSubState
{
	private var _txtTitle:FlxText;
	private var _txtServed:FlxText;
  private var _txtDefeated:FlxText;
	private var _txtTime:FlxText;

  public var served:Int=0;
  public var time:Float=0;
	
	override public function create():Void
	{
		super.create();

    var shade = new FlxSprite(0,0);
    shade.makeGraphic(1280, 720, 0xbb000000);
    add(shade);
		
		_txtTitle = new FlxText(0, 20, 0, "GAME\nOVER", 70);
		_txtTitle.alignment = CENTER;
		_txtTitle.screenCenter(FlxAxes.X);
		_txtTitle.borderStyle = FlxTextBorderStyle.OUTLINE_FAST;
		_txtTitle.borderColor = FlxColor.BLACK;
		_txtTitle.borderSize = 3;
		add(_txtTitle);

		_txtServed = new FlxText(0, 250, 0, "You served " + served + " customers!", 50);
		_txtServed.alignment = CENTER;
		_txtServed.screenCenter(FlxAxes.X);
		_txtServed.borderStyle = FlxTextBorderStyle.OUTLINE_FAST;
		_txtServed.borderColor = FlxColor.BLACK;
		_txtServed.borderSize = 3;
		add(_txtServed);

		_txtDefeated = new FlxText(0, 330, 0, "You defeated " + Customer.numDefeated + " enemies!", 50);
		_txtDefeated.alignment = CENTER;
		_txtDefeated.screenCenter(FlxAxes.X);
		_txtDefeated.borderStyle = FlxTextBorderStyle.OUTLINE_FAST;
		_txtDefeated.borderColor = FlxColor.BLACK;
		_txtDefeated.borderSize = 3;
		add(_txtDefeated);

		_txtTime = new FlxText(0, 400, 0, "You survived " + Std.int(time) + " seconds!", 50);
		_txtTime.alignment = CENTER;
		_txtTime.screenCenter(FlxAxes.X);
		_txtTime.borderStyle = FlxTextBorderStyle.OUTLINE_FAST;
		_txtTime.borderColor = FlxColor.BLACK;
		_txtTime.borderSize = 3;
		add(_txtTime);

    FlxG.sound.playMusic("assets/crowdmurmur.ogg");
	}


	override public function update(elapsed:Float):Void
  {
    super.update(elapsed);

    if(FlxG.keys.anyJustPressed([R, SPACE]))
    {
      FlxG.camera.fade(FlxColor.BLACK, .33, false, function()
      {
        FlxG.switchState(new TitleState());
      });
    }
  }
}