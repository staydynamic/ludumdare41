package;


import flixel.FlxG;
import flixel.FlxBasic;
import flixel.group.FlxGroup;

class EnemyManager extends FlxBasic{
	public static var customersServed = 0;

	public static var difficulty = 4;

	public static var entranceLocX:Int = 50;
	public static var entranceLocY:Int = 275 - 140; //floor height minus
                                                  // enemy height

	public var tables:FlxTypedGroup<Table>;

	private var _enemy:Customer;
	private var world:PlayState;
	private var _player:Player;
  private var _spawnPeriod:Float = 9;
  private var _countDown:Float = 1;

	public function new(game:PlayState, tablez:FlxTypedGroup<Table>, player:Player){
    super();
		this.world = game;
		this.tables = tablez;
		this._player = player;
	}


	public function createEnemy(){

		var table:Table;
    function hasNoCustomer(t:Table)
    {
      return t.getCustomer() == null;
    }
    // generate list of available tables
    var ts = tables.members.filter(hasNoCustomer);

		// as the number of customersServed increases so does the chance that they desire a food item
		var desires:Array<Bool> = [FlxG.random.bool(difficulty*customersServed), 
									FlxG.random.bool(difficulty*customersServed),
									FlxG.random.bool(difficulty*customersServed)];

		// customers must desire at least one food item
		desires[FlxG.random.int(0,2)] = true;

    // there are no empty tables
    if(ts.length == 0)
    {
      // give an arbitrary (unused) table
      table = tables.members[0];
		  // Create new sprite at the entrance
      _enemy = new Customer(entranceLocX, entranceLocY, table, desires, _player);
      _enemy.aggro();
    }
    else
    {
      table = ts[FlxG.random.int(0, ts.length -1)];
		  // Create new sprite at the entrance
      _enemy = new Customer(entranceLocX, entranceLocY, table, desires, _player);
      // tables track whom they're serving
      table.setCustomer(_enemy);
    }

		_enemy._MOVESPEED_X += FlxG.random.int(-3,3);
		_enemy._MOVESPEED_Y += FlxG.random.int(-3,3);

		// Add that sprite to the game
		world.addToWorld(_enemy);

		PlayState.gameMessage.text = "1";

		customersServed += 1;
	}

  override public function update(elapsed:Float)
  {
    _countDown -= elapsed;
    if(_countDown < 0 && Customer.numHostile < 50)
    {
      createEnemy();
      _countDown = _spawnPeriod;
      if(customersServed % 5 == 0 && _spawnPeriod > .5)
      {
        _spawnPeriod *= .9;
      }
    }
  }
}