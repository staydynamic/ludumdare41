package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxSpriteUtil;
import flixel.util.FlxColor;
import flixel.math.FlxMath;
import flixel.addons.util.FlxFSM;
import flixel.system.FlxSound;
import flixel.FlxObject;

class Customer extends FlxSprite
{
	public var fsm:FlxFSM<Customer>;
	public var targetX:Int = 0;
	public var targetY:Int = 0;
	public var table:Table;
	public var desires:Array<Bool>;
	public var _patience:Float=1;
	public var _punchReady:Float=2;
  public var _noTable:Bool = false;
	public var _recovered:Float=3;
	public var _MOVESPEED_X:Float = Util._CUSTOMER_MOVESPEED_X;
	public var _MOVESPEED_Y:Float = Util._CUSTOMER_MOVESPEED_Y;
	public var hitPoints:Int = 2;
	public var hit:Bool = false;
	public var _player:Player;
  private var _order:Null<FlxSprite> = null;
  public var _satiated:Bool = false;
  public var _hostile = false;
  public static var numHostile:Int;
  public static var numDefeated:Int;
  public static var customerSIZE:Int = 140;
  public var _HOWLONGSEATED:Int = 25;


  // Load sound files
  public static var _enterSfx:FlxSound;
  public static var _hitSfx:FlxSound;
  public static var _orderSfx:FlxSound;
  public static var _satisfiedSfx:FlxSound;
  public static var _punchSfx:FlxSound;

  public static function loadSfx():Void
  {
    _enterSfx = FlxG.sound.load("assets/customer-enters.ogg");
    _hitSfx = FlxG.sound.load("assets/hit-sound.ogg");
    _orderSfx = FlxG.sound.load("assets/new-order.ogg");
    _satisfiedSfx = FlxG.sound.load("assets/order-satisfied.ogg");
    _punchSfx = FlxG.sound.load("assets/punch-whiff.ogg");
  }

	public function new(X:Int, Y:Int, tab:Table, foods:Array<Bool>, player:Player)
	{
		super(X, Y);

		_player = player;

		var a:Int = FlxG.random.int(0,2);

		switch(a){
			case 0:
        		loadGraphic("assets/customer.png", true, customerSIZE, customerSIZE);
			case 1:
				loadGraphic("assets/customer1.png", true, customerSIZE, customerSIZE);
			case 2:
				loadGraphic("assets/customer2.png", true, customerSIZE, customerSIZE);
			default:
				loadGraphic("assets/customer.png", true, customerSIZE, customerSIZE);
		}

		animation.add("idle", [0, 1], 2);
		animation.add("walking", [2, 3], 2);
		animation.add("angry", [2, 3], 2);
		animation.add("seated", [0], 1);
		animation.add("hit", [6], 2);
		animation.add("punching", [5, 4], 2, false);
		animation.add("windingUpPunch", [4, 4, 4], 2, false);
		animation.add("knockedDown", [6, 7, 8, 9, 8, 9], 2, false);
		
		setFacingFlip(FlxObject.RIGHT, false, false);
		setFacingFlip(FlxObject.LEFT, true, false);
		facing = FlxObject.RIGHT;

		table = tab;
    setTarget(table.x, table.y);
		desires = foods;

		fsm = new FlxFSM<Customer>(this);
		fsm.transitions
			.add(AwaitingSeating, WalkingC, Conditionz.seatedByWaiter)
			.add(AwaitingSeating, Angry, Conditionz.noTable)
			.add(WalkingC, Seated, Conditionz.atTable)
			.add(Seated, Angry, Conditionz.patienceExpired)
			.add(Angry, WindingUp, Conditionz.windPunch)
			.add(WindingUp, Punching, Conditionz.punch)
			.add(WindingUp, KnockedDown, Conditionz.isHit)
			.add(Punching, Angry, Conditionz.punchingFinished)
			.add(Seated, Satiated, Conditionz.satiated)
			.add(Angry, KnockedDown, Conditionz.beaten)
			.add(KnockedDown, Angry, Conditionz.recovered)

			.start(AwaitingSeating);

		animation.play("idle");
	}

	public function getHit(){
		if(hit){
			return;
		}

		hitPoints -= 1;

		if(hitPoints == 0){
			this.kill();
      Customer.numHostile -= 1;
      Customer.numDefeated += 1;
		}

		hit = true;
	}

	public function setTarget(x:Float, y:Float)
	{
		targetX = Std.int(x);
		targetY = Std.int(y);
	}

  public function aggro():Void
  {
    _noTable = true;
  }

  public function placeOrder():FlxSprite
  {
    var order:FlxSprite = new FlxSprite(x, y - 35);
    var foods:Array<String> = ["assets/pizza.png",
                               "assets/sandwich.png",
                               "assets/soda.png"];
    var offset:Int = 5;
    order.makeGraphic(200, 55, FlxColor.WHITE, true);

    for(i in 0...3)
    {
      if(desires[i])
      {
        order.stamp(new FlxSprite(0,0,foods[i]), offset, 5);
        offset += 100;
      }
    }

    _order = order;

    return order;
  }

  public function clearOrder():Void
  {
    if(_order != null)
    {
      _order.kill();
    }
    _order = null;
  }

  public function hasNotOrdered():Bool
  {
    return _order == null;
  }

	override public function update(elapsed:Float):Void
	{
		fsm.update(elapsed);
		super.update(elapsed);

		if (x > FlxG.width - width - 4){
			x = FlxG.width - width - 4;
		}
		if (y > FlxG.height - height - 4){
			y = FlxG.height - height - 4;
		}
		if (y < 40){
			y = 40;
		}
		if (x < 4){
			x = 4;
		}
		super.update(elapsed);
	}
}

class Conditionz
{
	public static function seatedByWaiter(Owner:Customer):Bool
	{
		return (Owner._patience < 0);
	}
	public static function atTable(Owner:Customer):Bool
	{
		return (Std.int(Owner.targetX) == Std.int(Owner.x) 
			&& Std.int(Owner.targetY) == Std.int(Owner.y));
	}
	public static function patienceExpired(Owner:Customer):Bool
	{
		return (Owner._patience < 0);
	}
	public static function noTable(Owner:Customer):Bool
	{
		return Owner._noTable;
	}
	public static function punchingFinished(Owner:Customer):Bool
	{
		return Owner.animation.finished;
	}
	public static function satiated(Owner:Customer):Bool
	{
		return (!Owner.desires[0] && !Owner.desires[1] && !Owner.desires[2]);
	}
	public static function recovered(Owner:Customer):Bool
	{
		return (Owner._recovered < 0);
	}
	public static function beaten(Owner:Customer):Bool
	{
		return Owner.hit;
	}
	public static function windPunch(Owner:Customer):Bool
	{
		return ( Util.abs(Owner._player.x - Owner.x)<Util._PUNCH_DISTANCE &&
				 Util.abs(Owner._player.y - Owner.y)<Util._PUNCH_DISTANCE &&
				!(Owner._player.animation.name == "knockedDown"));
	}
	public static function punch(Owner:Customer):Bool
	{
		return Owner.animation.finished;
	}
	public static function isHit(Owner:Customer):Bool
	{
		return Owner.hit;
	}

}


class AwaitingSeating extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("idle");
    Customer._enterSfx.play();
		PlayState.gameMessage.text = "AwaitingSeating";
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner._patience -= elapsed;
	}
}

/*
 *  Customer is walking to table.  Given coords
 */ 
class WalkingC extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("walking");
		PlayState.gameMessage.text = "walking";
		owner._patience = owner._HOWLONGSEATED;
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		Util.moveToTarget(owner);
	}
}

class Seated extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("seated");
    Customer._orderSfx.play();
		PlayState.gameMessage.text = "seated";
		owner.velocity.x = 0;
		owner.velocity.y = 0;
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner._patience -= elapsed;
	}

  override public function exit(owner:Customer):Void
  {
    owner.table.resetCustomer();
    owner.clearOrder();
  }
}

class KnockedDown extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.hit = false;
		owner._recovered = 3;
		owner.animation.play("knockedDown");
		PlayState.gameMessage.text = "knockedDown";
		owner.velocity.x = 0;
		owner.velocity.y = 0;
		
		// Blow the enemy back
		// is the player to the right of us
		if( owner._player.x > owner.x){
			owner.velocity.x -= 200;
		}
		else{
			owner.velocity.x += 200;
		}

    Customer._hitSfx.play();
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner._recovered -= elapsed;
		owner.velocity.x = (owner.velocity.x > 0) ? owner.velocity.x - elapsed*Util._CUSTOMER_KNOCKBACK_DIST :
													owner.velocity.x + elapsed*Util._CUSTOMER_KNOCKBACK_DIST;
	}
}


/*
 *  Customer is walking to table.  Given coords
 */ 
class Angry extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("angry");
		owner.setTarget(PlayState.getPlayer().x, PlayState.getPlayer().y);
    if(!owner._hostile)
    {
      owner._hostile = true;
      Customer.numHostile += 1;
    }

		PlayState.gameMessage.text = "angry";
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		if(owner._player.animation.name == "knockedDown"){
			owner.setTarget(owner.x, owner.y);
		}
		else{
			owner.setTarget(owner._player.x, owner._player.y);
		}
		
		Util.moveToTarget(owner);
	}
}


class WindingUp extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("windingUpPunch");
		PlayState.gameMessage.text = "windingUpPunch";
		owner.velocity.x = 0;
		owner.velocity.y = 0;
	}
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{

	}
}


class Punching extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("punching");
    Customer._punchSfx.play();
		PlayState.gameMessage.text = "punching";
		owner.velocity.x = 0;
		owner.velocity.y = 0;
	}
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{

	}
}

/*
 *  Customer is satiated.  Walk out of restaurant
 */ 
class Satiated extends FlxFSMState<Customer>
{
	override public function enter(owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		owner.animation.play("walking");
		owner._satiated = true;
		PlayState.gameMessage.text = "satiated";
    Customer._satisfiedSfx.play();
		owner.setTarget(PlayState._entranceX, PlayState._entranceY + 150);
	}
	
	override public function update(elapsed:Float, owner:Customer, fsm:FlxFSM<Customer>):Void 
	{
		Util.moveToTarget(owner);
	}
}