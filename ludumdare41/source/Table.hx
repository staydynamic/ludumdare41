package;

import flixel.FlxG;
import flixel.FlxSprite;


class Table extends FlxSprite
{
  private var _customer:Null<Customer> = null;
  private var _TABLE_IMAGE_X:Int = 180;
  private var _TABLE_IMAGE_Y:Int = 180;

  public function setCustomer(customer:Customer):Void
  {
    _customer = customer;
  }

  public function getCustomer():Customer
  {
    return _customer;
  }

  public function getCustomerDesires():Array<Bool>
  {
    return _customer.desires;
  }

  public function resetCustomer():Void
  {
    _customer = null;
  }

	public function new(X:Int, Y:Int)
	{
    super(X, Y);
    loadGraphic("assets/table.png", true, _TABLE_IMAGE_X, _TABLE_IMAGE_Y);

    animation.add("table", [0], 1);
    animation.play("table");
	}
}