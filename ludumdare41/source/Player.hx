package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxSpriteUtil;
import flixel.addons.util.FlxFSM;
import flixel.system.FlxSound;
import flixel.FlxObject;


class Player extends FlxSprite
{
	public var fsm:FlxFSM<Player>;
	public var isHit:Bool = false;
	public var _recovered:Float=Util._PLAYER_RECOVERY_SPEED;
	public var hitPoints:Int=Util._PLAYER_HITPOINTS;
	public var _invuln:Float=Util._PLAYER_INVULN;
	public var _isVulnerable:Bool=true;

  // sound effects
  private static var _deadSfx:FlxSound;

  public static function loadSfx():Void
  {
    _deadSfx = FlxG.sound.load("assets/u-died.ogg");
  }

  /**
   * Track which foodstuffs the player is holding
   */
  public var hasFood:Array<Bool> = [false, false, false];

  public function wantsFood(foodType:Int):Bool
  {
    var count:Int = 0;
    for (i in 0...3)
    {
      if(i == foodType && hasFood[i])
      {
        return false;
      }
      else if(hasFood[i])
      {
        count++;
      }
    }
    return count < 2;
  }

	public function new()
	{
		super(100, 100);
		loadGraphic("assets/girl.png", true, 140, 140);

		animation.add("stand", [0, 1], 4);
		animation.add("walking", [2, 3], 3);
		animation.add("swing", [4, 5], 3, false);
		animation.add("knockedDown", [6, 7], 3, false);

		animation.play("walking");

		setFacingFlip(FlxObject.RIGHT, false, false);
		setFacingFlip(FlxObject.LEFT, true, false);
		facing = FlxObject.RIGHT;

		fsm = new FlxFSM<Player>(this);
		fsm.transitions
			.add(Standing, Walking, Conditions.walk)
			.add(Standing, Swing, Conditions.swing)
			.add(Walking, Swing, Conditions.swing)
			.add(Walking, Standing, Conditions.notwalk)
			.add(Swing, Walking, Conditions.animationFinished)
			.add(Standing, KnockedDown_Player, Conditions.hit)
			.add(Walking, KnockedDown_Player, Conditions.hit)
			.add(Swing, KnockedDown_Player, Conditions.hit)
			.add(KnockedDown_Player, Standing, Conditions.recovered)
			.start(Standing);
	}

	public function getHit( waiterOnRight:Bool){
		if(isHit){ return; }

		hitPoints -= 1;

		if(hitPoints == 0){
			this.kill();
      Player._deadSfx.play();
			//Game over
		}

		if(waiterOnRight){ velocity.x += 200; }
		else{ velocity.x -= 200; }
		isHit = true;
	}


	override public function update(elapsed:Float):Void
	{
		fsm.update(elapsed);
		super.update(elapsed);

		if (x > FlxG.width - width - 4){
			x = FlxG.width - width - 4;
		}
		if (y > FlxG.height - height - 4){
			y = FlxG.height - height - 4;
		}
		if (y < 275 - frameHeight){
			y = 275 - frameHeight;
		}
		if (x < 4){
			x = 4;
		}
		super.update(elapsed);
	}
}

class Conditions
{
	public static function swing(Owner:Player):Bool
	{
		return (FlxG.keys.justPressed.SPACE);
	}
	public static function animationFinished(Owner:Player):Bool
	{
		return Owner.animation.finished;
	}
	public static function walk(Owner:Player):Bool
	{
		return (FlxG.keys.anyPressed([LEFT, UP, DOWN, RIGHT, W, A, S, D]));
	}
	public static function notwalk(Owner:Player):Bool
	{
		return !(FlxG.keys.anyPressed([LEFT, UP, DOWN, RIGHT, W, A, S, D]));
	}
	public static function hit(Owner:Player):Bool
	{
		return Owner.isHit;
	}
	public static function recovered(Owner:Player):Bool
	{
		if((Owner._recovered < 0)){
			Owner.isHit = false;
			return true;
		}
		else{
			return false;
		}
	}
}


class Standing extends FlxFSMState<Player>
{
	override public function enter(owner:Player, fsm:FlxFSM<Player>):Void 
	{
		owner.animation.play("stand");
	}
	
	override public function update(elapsed:Float, owner:Player, fsm:FlxFSM<Player>):Void 
	{
		if(owner._invuln>0){
			owner._invuln -= elapsed;
		}
	}
}


class Walking extends FlxFSMState<Player>
{
	override public function enter(owner:Player, fsm:FlxFSM<Player>):Void 
	{
		owner.animation.play("walking");
	}
	
	override public function update(elapsed:Float, owner:Player, fsm:FlxFSM<Player>):Void 
	{
		Util.checkMove(owner);
		if(owner._invuln>0){
			owner._invuln -= elapsed;
		}
	}
}


class Swing extends FlxFSMState<Player>
{
	override public function enter(owner:Player, fsm:FlxFSM<Player>):Void 
	{
		owner.animation.play("swing");
    Customer._punchSfx.play();
	}
	
	override public function update(elapsed:Float, owner:Player, fsm:FlxFSM<Player>):Void 
	{
		Util.checkMove(owner);
		if(owner._invuln>0){
			owner._invuln -= elapsed;
		}
	}
	
}

class KnockedDown_Player extends FlxFSMState<Player>
{
	override public function enter(owner:Player, fsm:FlxFSM<Player>):Void 
	{
		owner.animation.play("knockedDown");
		owner.isHit = true;
		owner._recovered = Util._PLAYER_RECOVERY_SPEED;
		owner._invuln=Util._PLAYER_INVULN;
    Customer._hitSfx.play();
	}
	
	override public function update(elapsed:Float, owner:Player, fsm:FlxFSM<Player>):Void 
	{
		owner._recovered -= elapsed;
		owner.velocity.x = (owner.velocity.x > 0) ? owner.velocity.x - elapsed*Util._PLAYER_KNOCKBACK_DIST :
													owner.velocity.x + elapsed*Util._PLAYER_KNOCKBACK_DIST;
	}
}