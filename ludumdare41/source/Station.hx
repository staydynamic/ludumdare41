
package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxSound;

/**
 * Class declaration for the base customer
 */
class Station extends FlxSprite
{
  private var COOLDOWN:Float = 3;
  /**
   * How much time remains until more food is available
   */
  private var _cooldown:Float = 0;
  /**
   * Which foodstuff the station produces
   */
  public var foodType:Int;

  private var _dingPlayed:Bool = true;
  private static var _readySfx:FlxSound;

  public static function loadSfx():Void
  {
    _readySfx = FlxG.sound.load("assets/new-bread.ogg");
  }
	
	/**
	 * This is the constructor for the squid monster.
	 * We are going to set up the basic values and then create a simple animation.
	 */
	public function new(X:Int, Y:Int, FoodType:Int)
	{
		// Initialize sprite object
		super(X, Y);

    foodType = FoodType;

    loadGraphic("assets/stations_scaled.png", true, 177, 100);
    switch(foodType) {
      case 0:
      // pizza
        
        animation.add("preparing", [1,2], 15);
        animation.add("ready", [3]);
      case 1:
      // sandwich
        animation.add("preparing", [2,1], 15);
        animation.add("ready", [4]);
      default:
      // soda
        animation.add("preparing", [2,1], 15);
        animation.add("ready", [5]);
    }

    animation.play("ready");
	}
	
	override public function update(elapsed:Float):Void
	{

		if (_cooldown > 0)
		{
      _cooldown -= elapsed;
		}

    if (_cooldown <= 0)
    {
      animation.play("ready");
      if(!_dingPlayed)
      {
        Station._readySfx.play();
        _dingPlayed = true;
      }
    }
		
		super.update(elapsed);
	}
	
	public function resetCooldown():Void
	{
    _dingPlayed = false;
    animation.play("preparing");
		_cooldown = COOLDOWN;
	}
}